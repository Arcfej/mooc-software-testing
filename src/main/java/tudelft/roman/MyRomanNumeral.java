package tudelft.roman;

import java.util.HashMap;
import java.util.Map;

public class MyRomanNumeral {

    /*
    Possible test cases:

    - normal number with increasing digits
    - normal number with extraction digits
    - upper or lower case
    - number with wrong digits e.g. 'K'
    - too many same digits after each-other e.g. "MXXXXX"
    - right digits in wrong order e.g.e.g. "ICDM"
     */

    private static Map<Character, Integer> romanDigits = new HashMap<>();
    static {
        romanDigits.put('I', 1);
        romanDigits.put('V', 5);
        romanDigits.put('X', 10);
        romanDigits.put('L', 50);
        romanDigits.put('C', 100);
        romanDigits.put('D', 500);
        romanDigits.put('M', 1000);
    }

    public int convert(String romanNumeral) {
        romanNumeral = romanNumeral.toUpperCase();
        for (char digit : romanDigits.keySet()) {
            if (romanNumeral.contains(new String(new char[]{digit, digit, digit, digit}))) {
                // Return 0 if it's contains to many of the same digits after each-other
                return 0;
            }
        }
        int[] digits = new int[romanNumeral.length()];
        for (int i = 0; i < romanNumeral.length(); i++) {
            Integer digit = romanDigits.get(romanNumeral.charAt(i));
            // Return 0 if it's contains a character which is not a roman digit
            if (digit == null) return 0;
            else digits[i] = digit;
        }
        int number = 0;
        for (int i = 0; i < digits.length; i++) {
            int digit = digits[i];
            int nextDigit = (i < digits.length - 1) ? digits[i + 1] : 0;
            int thirdDigit = (i < digits.length - 2) ? digits[i + 2] : 0;
            if (digit == nextDigit && digit < thirdDigit) return 0;
            if (digit < nextDigit) {
                if (nextDigit - digit != 4 * digit) return 0;
                else number -= digit;
            } else number += digit;
        }
        return number;
    }
}