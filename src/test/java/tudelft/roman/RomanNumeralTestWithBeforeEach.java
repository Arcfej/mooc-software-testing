package tudelft.roman;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class RomanNumeralTestWithBeforeEach {

    private MyRomanNumeral roman;

    @BeforeEach
    public void initialize() {
        this.roman = new MyRomanNumeral();
    }

    @Test
    public void singleNumber() {
        int result = roman.convert("I");
        Assertions.assertEquals(1, result);
    }

    @Test
    public void numberWithManyDigits() {
        int result = roman.convert("VIII");
        Assertions.assertEquals(8, result);
    }

    @Test
    public void numberWithSubtractiveNotation() {
        int result = roman.convert("IV");
        Assertions.assertEquals(4, result);
    }

    @Test
    public void numberWithAndWithoutSubtractiveNotation() {
        int result = roman.convert("XLIV");
        Assertions.assertEquals(44, result);
    }

    @Test
    public void numberWithWrongCharacters() {
        int result = roman.convert("KOPTIVCM");
        Assertions.assertEquals(0, result);
    }

    @Test
    public void numberWithWrongOrderOfDigits() {
        int result = roman.convert("IVXLCDM");
        Assertions.assertEquals(0, result);
    }

    @Test
    public void numberWithTooManyOfTheSameDigitsAfterEachOther() {
        int result = roman.convert("MMMMDDDDCCCC");
        Assertions.assertEquals(0, result);
    }

    @Test
    public void numberWithTooManySubtraction() {
        int result = roman.convert("IIIV");
        Assertions.assertEquals(0, result);
    }

    @Test
    public void numberWithReverseOrder() {
        int result = roman.convert("LXDMC");
        Assertions.assertEquals(0, result);
    }
}
